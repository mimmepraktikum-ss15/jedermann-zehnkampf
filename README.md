# README #

# mimmepraktikum/jedermann-zehnkampf.git

# Jedermannzehnkampf Userwebsite

##
##General
the jedermannzehnkampf website should inform the user about the SWC Regensburg Jedermannzehnkampf, which takes place once a year. Furthermore users have the possibility to create their own account to use all features of the website. Therefor the user has to register on the "Anmeldung" page. 
The page consists of the homescreen "Home", "Disziplinen", "Score", "Trainingsplan", "Bestenliste", "Infos zum Wettkampf" and "Anmeldung".
In "Disziplinen" you get Informations abouts each discipline that has to be completed at a championship. In "Score" you can type your own scores that you've reached so far and calculate the points you would make as a woman or man. Additionally you have the possibilty to get a visualization of your distribution of points according to each discipline. If you are logged in as a user, your scores will be saved by calculating the points. In "Trainingsplan" the user has access to a whole training schedule and be prepaired for the championship. logged in useres can save their fitness level and add notices. "Bestenliste" gives you a clear view of each 10 best participants of last year - woman and men - and the points they've reached. For a quick classification it shows your points below, that you have saved before by calculating the points. "Infos zum Wettkampf" shows every Information needed to know for the Jedermannzehnkampf. And at the end in "Anmeldung" you can register, log in or get to know how to apply for the championship. To log out there is a button in the right top corner.

##
##version
version 3.0: endversion

##
##Browser support

Regularly tested in Chrome. Application should run on most modern browsers.

##
##Running

The application starts by opening the index.html in your browser.

##
##Developement

Application built with HTML, CSS, Javascript, JSON, jQuery, Userapp.io

##
##Folders structure

One main folder jedermann-zehnkampf, that contains subfolders res, js, README.md and the index.html.

res contains subfolders html, css, fonts, imgs.

js contains subfolders src and libs

src holds all the javascript files: 
 - TrainingsplanController.js
 - SignUp.js
 - ScoreCalculator.js
 - LogOut.js
 - LogIn.js
 - DisziplinenController.js
 - Default.js
 - Datamodel.js

libs holds all the external libraries needed for the application.
