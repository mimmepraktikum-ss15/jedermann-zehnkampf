var App = App || {};
App.Datamodel = function () {
    "use strict";
    /* eslint-env browser, jquery */
    
    /* gespeicherte Infos:
    
    - checkboxen Trainingsplan
    - Leistungen Score
    
    */
    
    function scoreUpload(dataArr) {
        var tok = Cookies.get("ua_session_token");
        UserApp.setToken(tok);
        
        UserApp.User.save({
                    user_id: "self",
                    properties: {
                        scores: {
                            value: dataArr,
                            override: true
                        }
                    }
                });
        
        UserApp.User.get({ user_id: "self" }, function(error, user) {
            console.log(user);
        });
    }

    
    function trainingDataUpload(dataArr) {
        var tok = Cookies.get("ua_session_token");
        UserApp.setToken(tok);
        
        UserApp.User.save({
                    user_id: "self",
                    properties: {
                        checkboxes: {
                            value: dataArr,
                            override: true
                        }
                    }
                });
        
        UserApp.User.get({ user_id: "self" }, function(error, user) {
            console.log(user);
        });
    }
    
    return {
        scoreUpload: scoreUpload,
        trainingDataUpload: trainingDataUpload
    };
    
    
    
};
