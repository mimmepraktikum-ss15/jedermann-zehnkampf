window.Default = (function () {
    "use strict";
    /* eslint-env browser, jquery  */
    var scoreCalc,
        dataModel,
        trainingController,
        disziplinen,
        logOut,
        logIn,
        highscores;
    
    function init() {
        UserApp.initialize({
            appId: "55b5f9e753560"
        });

        scoreCalc = new App.ScoreCalculator(document.querySelectorAll("#score"));
        scoreCalc.init();
        logOut = new App.LogOut(document.querySelectorAll("#logout"));
        logOut.init();
        dataModel = new App.Datamodel();
        disziplinen = new App.Disziplinen();
        disziplinen.init();
        logIn = new App.LogIn();
        logIn.init();  
        trainingController = new App.TrainingsplanController(document.querySelectorAll("#trainingsplan"));
        trainingController.init();
        
        
    }
    
    //<!----- JQUERY FOR SLIDING NAVIGATION --->   
    $(document).ready(function () {
        $('a[href*=#]').each(function () {
            if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'')
                    && location.hostname == this.hostname
                    && this.hash.replace(/#/,'') ) {
                var $targetId = $(this.hash), $targetAnchor = $('[name=' + this.hash.slice(1) +']');
      var $target = $targetId.length ? $targetId : $targetAnchor.length ? $targetAnchor : false;
       if ($target) {
         var targetOffset = $target.offset().top;

//<!----- JQUERY CLICK FUNCTION REMOVE AND ADD CLASS "ACTIVE" + SCROLL TO THE #DIV--->   
                    $(this).click(function () {
                        $("#nav li a").removeClass("active");
                        $(this).addClass('active');
                        $('html, body').animate({scrollTop: targetOffset}, 1000);
                        document.getElementById("disziplinenInfo").innerHTML = "";
                        document.getElementById("trainingseinheit").innerHTML = "";
                        return false;
                    });
                }
            }
        });
    });
    
    return {
        init: init
    };
    
}());
