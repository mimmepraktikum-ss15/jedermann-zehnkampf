var App = App || {};
App.Disziplinen = function () {
    "use strict";
    
    var hundredMeter,
        longJump,
        shotPut,
        highJump,
        fourhundredMeter,
        hurdling,
        discusThrow,
        poleVault,
        javelinThrow,
        thousandfivehundredMeter;

    
    function init() {
        hundredMeter = $('#100');
        hundredMeter.on("click", hundredMeterInfo);
        
        longJump = $('#weitsprung');
        longJump.on("click", longJumpInfo);
        
        shotPut = $('#kugel');
        shotPut.on("click", shotPutInfo);
        
        highJump = $('#hochsprung');
        highJump.on("click", highJumpInfo);
        
        fourhundredMeter = $('#400');
        fourhundredMeter.on("click", fourhundredMeterInfo);
        
        hurdling = $('#hürden');
        hurdling.on("click", hurdlingInfo);
        
        discusThrow = $('#diskus');
        discusThrow.on("click", discusThrowInfo);
        
        poleVault = $('#stabhochsprung');
        poleVault.on("click", poleVaultInfo);
        
        javelinThrow = $('#speer');
        javelinThrow.on("click", javelinThrowInfo);
        
        thousandfivehundredMeter = $('#1500');
        thousandfivehundredMeter.on("click", thousandfivehundredMeterInfo);
        
        document.getElementById("disziplinenInfo").innerHTML = "";
    }
    
    function hundredMeterInfo(event) {
        document.getElementById("disziplinenInfo").innerHTML = "Der 100-Meter-Lauf ist eine Sprintdisziplin in der Leichtathletik und wird auf einer geraden Strecke ausgetragen, wobei jeder Läufer vom Start bis zum Ziel in seiner eigenen Bahn bleiben muss. Gestartet wird im Tiefstart mit Hilfe von Startblöcken. Bei den Olympischen Sommerspielen ist die 100-Meter-Strecke die kürzeste Sprintdistanz.";
    }
    
    function longJumpInfo(event) {
        document.getElementById("disziplinenInfo").innerHTML = "Als Weitsprung bezeichnet man eine olympische Disziplin der Leichtathletik, in der ein Sportler versucht, nach einem Anlauf mittels eines einzelnen Sprungs eine möglichst große Weite zu erzielen.";
    }
    
    function shotPutInfo(event) {
        document.getElementById("disziplinenInfo").innerHTML = "Kugelstoßen (auch Kugelstoß) ist eine Wurfdisziplin der Leichtathletik, bei der eine Metallkugel durch explosionsartiges Strecken des Arms möglichst weit gestoßen wird.";
    }
    
    function highJumpInfo(event) {
        document.getElementById("disziplinenInfo").innerHTML = "Hochsprung ist eine Disziplin in der Leichtathletik, bei der ein Athlet oder eine Athletin versucht, beim Sprung über eine Latte die größtmögliche Höhe zu erzielen.";
    }
    
    function fourhundredMeterInfo(event) {
        document.getElementById("disziplinenInfo").innerHTML = "Der 400-Meter-Lauf ist die längste Sprintdisziplin in der Leichtathletik und wird sowohl bei Freiluft- als auch bei Hallenwettkämpfen ausgetragen.";
    }
    
    function hurdlingInfo(event) {
        document.getElementById("disziplinenInfo").innerHTML = "Der 110-Meter-Hürdenlauf ist eine olympische Disziplin der Leichtathletik für Männer. Dabei sind auf einer geraden 110-Meter-Strecke zehn 1,067 Meter hohe, in gleichen Abständen aufgestellte Hürden zu überlaufen.";
    }
    
    function discusThrowInfo(event) {
        document.getElementById("disziplinenInfo").innerHTML = "Der Diskuswurf (auch Diskuswerfen) ist eine olympische Disziplin der Leichtathletik, bei der eine linsenförmige Scheibe möglichst weit zu werfen ist.";
    }
    
    function poleVaultInfo(event) {
        document.getElementById("disziplinenInfo").innerHTML = "Stabhochsprung ist eine Disziplin in der Leichtathletik, bei der die Springer nach ihrem Anlauf eine hochliegende Sprunglatte mit Hilfe eines langen, flexiblen Stabes überwinden. Diese Latte ist 4,50 Meter lang und so auf zwei Sprungständern gelagert, dass sie bei leichter Berührung herunterfällt.";
    }
    
    function javelinThrowInfo(event) {
        document.getElementById("disziplinenInfo").innerHTML = "Speerwurf (Speerwerfen, englisch javelin throw) ist eine Disziplin der Leichtathletik, bei der ein Speer nach einem Anlauf möglichst weit zu werfen ist. Dafür stehen im Wettkampf sechs Versuche zur Verfügung.";
    }
    
    function thousandfivehundredMeterInfo(event) {
        document.getElementById("disziplinenInfo").innerHTML = "Der 1500-Meter-Lauf ist ein Bahnwettkampf der Leichtathletik. Zu laufen sind auf einer 400-Meter-Bahn zunächst 300 Meter einer Runde und dann drei volle Stadionrunden. Gestartet wird nach der ersten Kurve im Stehen (Hochstart) von einer gekrümmten Startlinie (Evolvente).";
    }
   
    return {
        init: init
    };
};