//userapp.client.js

var App = App || {};
App.LogIn = function () {
    "use strict";

    var downloadArray,
        downloadedScoresArray,
        downloadedCheckboxesArray,
        trainingsplanController,
        scoreCalculator,
        logInButton,
        highscores;
    
    
    function init() {
        trainingsplanController = App.TrainingsplanController();
        scoreCalculator = App.ScoreCalculator();
        
        logInButton = $('#logInButton');
        logInButton.on("click", login);
    }

    
    window.onload = function () {
        var show = localStorage.getItem("showLogoutButton");
        if (show === 'true') {
            document.getElementById("logout").style.visibility = "visible";
        }
    }


    function login() {
			
			// This will authenticate the user
        UserApp.User.login({
            login: document.getElementById("usernameLogin").value,
            password: document.getElementById("passwordLogin").value

        }, function (error, result) {
            if (error) {
				alert("Error: " + "Falsches Passwort");
					
            } else {
                onLoginSuccessful();
                document.getElementById("successfulLogIn").style.visibility = "visible";
                setTimeout(function() {
                    document.getElementById("successfulLogIn").style.visibility = "hidden";
                },3000);
            }
        });
        return false;
    }

    
    function download() {
        var tok = Cookies.get("ua_session_token");
        UserApp.setToken(tok);

        UserApp.User.get({
            user_id: "self"
        }, function (error, user) {
            if (error) {
                alert("Error: " + error.message);
            } else {
                var jsonParse = user[0],
                    userName = jsonParse.first_name;

                downloadedScoresArray = jsonParse.properties.scores.value;
                downloadedCheckboxesArray = jsonParse.properties.checkboxes.value;
                var parsedCheckboxes = JSON.parse(downloadedCheckboxesArray),
                    parsedScores = JSON.parse(downloadedScoresArray);

                trainingsplanController.setBoxes(parsedCheckboxes);
                scoreCalculator.setScores(parsedScores);
                scoreCalculator.saveScoreToVariable();
            }
        }
            );
    }


    function onLoginSuccessful() {
        Cookies.set("ua_session_token", UserApp.global.token);

        download();
            
        showLogoutButton();

			// Redirect the user to the index page
			window.location = "index.html#home";
    }

    function showLogoutButton() {
        document.getElementById("logout").style.visibility = "visible";
        localStorage.setItem("showLogoutButton", "true"); //store state in localStorage
    }



    return {
        init: init
    };
};