
var App = App || {};
App.LogOut = function () {
    "use strict";
    /* eslint-env browser, jquery */

    var logoutButton,
        trainingsplanController,
        scoreCalculator;


    
    function init() {
        trainingsplanController = App.TrainingsplanController();
        scoreCalculator = App.ScoreCalculator();
        logoutButton = $('#logout');
        logoutButton.on("click", logout);
    }
    
    function logout(event) {
        var tok = Cookies.get("ua_session_token");
        UserApp.setToken(tok);

        UserApp.User.logout(function (error, result) {
            if (error) {
                alert("Error: " + error.message);
            } else {
                document.getElementById("successfulLogOut").style.visibility = "visible";
                setTimeout(function() {
                    document.getElementById("successfulLogOut").style.visibility = "hidden";
                },3000);
                trainingsplanController.resetBoxes();
                scoreCalculator.clearScores();
                clearUserData();
            }
        });
    }
    
    function clearUserData() {
        document.getElementById("usernameLogin").value = "";
        document.getElementById("passwordLogin").value = "";
        document.getElementById("disziplinenInfo").innerHTML = "";
        document.getElementById("trainingseinheit").innerHTML = "";
    }
    
     return {
        init: init
    };
};