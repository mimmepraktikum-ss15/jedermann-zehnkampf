var App = App || {};
App.ScoreCalculator = function (score) {
    "use strict";
    /* eslint-env browser, jquery */

    var mCalcButton,
        wCalcButton,
        scoreButton,
        grafikButton,
        totalscoreW,
        totalscoreM,
        dataModel,
        highscore,
        scoreArr,
        scoresProp,
    
        sprint,
        Weitsprung,
        Kugelstoß,
        Hochsprung,
        Lauf400,
        Hürden, 
        Diskuswurf, 
        Stabhochsprung, 
        Speerwurf,
        Lauf1500,
    
        PunkteSprint ,
        PunkteWeitsprung,
        PunkteKugelstoß,
        PunkteHochsprung,
        PunkteLauf400,
        PunkteHürden,
        PunkteDiskuswurf,
        PunkteStabhochsprung,
        PunkteSpeerwurf,
        PunkteLauf1500;
    
    
function init() { 
    dataModel = App.Datamodel();
    
    mCalcButton = $('#mScoreButton');
    mCalcButton.on("click", onMCalcButtonClicked);   
    
    wCalcButton = $('#wScoreButton');
    wCalcButton.on("click", onWCalcButtonClicked);  
    
    google.load("visualization", "1", {packages:["corechart"]});
    google.setOnLoadCallback(initialize);
}
    
    

/* Formel:

Punkte für Laufwettbewerbe:A mal (B - x)^C
Punkte für Wurf- und Sprungwettbewerbe:A mal (x - B)^C

*/
    
    
    

        

    
      function drawPieChart() {

        var data = google.visualization.arrayToDataTable([
          ['Disziplin', 'Score'],
          ['100m Sprint', PunkteSprint],
          ['Weitsprung' , PunkteWeitsprung],
          ['Kugelstoßen' , PunkteKugelstoß], 
          ["Hochsprung" , PunkteHochsprung],
          ["400m Lauf" , PunkteLauf400],
          ["Hürdenlauf" , PunkteHürden], 
          ["Diskuswurf" , PunkteDiskuswurf], 
          ["Stabhochsprung" , PunkteStabhochsprung], 
          ["speerwurf" , PunkteSpeerwurf],
          ["1500m Lauf" , PunkteLauf1500]
        ]);

        var options = {
          title: 'Meine Leistungen',
            
        };

        var chart = new google.visualization.PieChart(document.getElementById("piechart"));

        chart.draw(data, options);
      }

    function initialize () {
    $('#grafikGenButton').unbind("click").click(function() {
        drawPieChart();
        $('html, body').animate({scrollTop: $('#piechart').offset().top - 200}, 700);
    });
}
    
function jumpThrowFormula(a, b, c, val) {
    return a * (Potenz((val-b), c));
}
    
function runningFormula(a, b, c, val) {
    return a * (Potenz((b-val), c));
}
   
 
function Potenz(z,c) {
    return Math.pow(z,c);
}

    
function onMCalcButtonClicked (event) {
    document.querySelector('[name="gesamtwert"]').value = "";
    calculateMen();
    document.querySelector('[name="gesamtwert"]').value = totalscoreM;
    $('#grafikGenButton').css('visibility', 'visible');
    saveScore();
    var scoreUp = getScore();
    dataModel.scoreUpload(scoreUp);
    document.getElementById("scoreUserM").value = totalscoreM;
}
 
    
 
function onWCalcButtonClicked (event) {
    document.querySelector('[name="gesamtwert"]').value = "";
    calculateWoman();
    document.querySelector('[name="gesamtwert"]').value = totalscoreW;
    $('#grafikGenButton').css('visibility', 'visible');
    saveScore();
    var scoreUp = getScore();
    dataModel.scoreUpload(scoreUp);
    document.getElementById("scoreUserW").value = totalscoreW;
}
    
    
function saveScore() {
    scoreArr = {"scores" : [{ "sprint" : sprint },
               { "weit" : Weitsprung }, 
               { "kugel" :Kugelstoß }, 
               { "hoch" : Hochsprung },
               { "m400" : Lauf400 },
               { "hürden" : Hürden}, 
               { "diskus" : Diskuswurf }, 
               { "stab" : Stabhochsprung }, 
               { "speer" : Speerwurf },
               { "m1500" : Lauf1500 }
                ]};
}
    
function getScore () {
    
    scoresProp = JSON.stringify(scoreArr);
    return scoresProp;
}
    
    function setScores(parsedScores) {
        document.getElementById("ID100m").value = parsedScores.scores[0].sprint;
        document.getElementById("ID_long_jump_distance").value = parsedScores.scores[1].weit;
        document.getElementById("ID_shot_put_distance").value = parsedScores.scores[2].kugel;
        document.getElementById("ID_high_jump_distance").value = parsedScores.scores[3].hoch;
        document.getElementById("ID_400m_time").value = parsedScores.scores[4].m400;
        document.getElementById("ID_110m_hurdling_time").value = parsedScores.scores[5].hürden;
        document.getElementById("ID_discus_throw_distance").value = parsedScores.scores[6].diskus;
        document.getElementById("ID_pole_vault_distance").value = parsedScores.scores[7].stab;
        document.getElementById("ID_javelin_throw_distance").value = parsedScores.scores[8].speer;
        document.getElementById("ID_1500m_time").value = parsedScores.scores[9].m1500;
    }
    
    function clearScores() {
        document.getElementById("ID100m").value = "";
        document.getElementById("ID_long_jump_distance").value = "";
        document.getElementById("ID_shot_put_distance").value = "";
        document.getElementById("ID_high_jump_distance").value = "";
        document.getElementById("ID_400m_time").value = "";
        document.getElementById("ID_110m_hurdling_time").value = "";
        document.getElementById("ID_discus_throw_distance").value = "";
        document.getElementById("ID_pole_vault_distance").value = "";
        document.getElementById("ID_javelin_throw_distance").value = "";
        document.getElementById("ID_1500m_time").value = "";
        document.querySelector('[name="gesamtwert"]').value = "";
        document.querySelector('[name="100m_score"]').value = "";
        document.querySelector('[name="long_jump_score"]').value = "";
        document.querySelector('[name="shot_put_score"]').value = "";
        document.querySelector('[name="high_jump_score"]').value = "";
        document.querySelector('[name="400m_score"]').value = "";
        document.querySelector('[name="110m_hurdling_score"]').value = "";
        document.querySelector('[name="discus_throw_score"]').value = "";
        document.querySelector('[name="pole_vault_score"]').value = "";
        document.querySelector('[name="javelin_throw_score"]').value = "";
        document.querySelector('[name="1500m_score"]').value = "";
    }
    
    function saveScoreToVariable () {
        sprint = document.querySelector('[name="100m_time"]').value,
        Weitsprung = document.querySelector('[name="long_jump_distance"]').value, 
        Kugelstoß = document.querySelector('[name="shot_put_distance"]').value, 
        Hochsprung = document.querySelector('[name="high_jump_distance"]').value,
        Lauf400 = document.querySelector('[name="400m_time"]').value,
        Hürden = document.querySelector('[name="110m_hurdling_time"]').value, 
        Diskuswurf = document.querySelector('[name="discus_throw_distance"]').value, 
        Stabhochsprung = document.querySelector('[name="pole_vault_distance"]').value, 
        Speerwurf = document.querySelector('[name="javelin_throw_distance"]').value,
        Lauf1500 = document.querySelector('[name="1500m_time"]').value;
       
    }

    
function calculateWoman() {
    
    
    var aWsprint = 17.857 ,
        aWWeitsprung = 0.188807 ,
        aWKugelstoß = 56.0211 ,
        aWHochsprung = 1.84523 ,
        aWLauf400 = 1.34285 ,
        aWHürden = 9.23076 ,
        aWDiskuswurf = 12.3311 ,
        aWStabhochsprung = 0.44125 ,
        aWSpeerwurf = 15.9803 ,
        aWLauf1500 = 0.02883 ,
    
        bWsprint = 21.0 ,
        bWWeitsprung = 210 ,
        bWKugelstoß = 1.5 ,
        bWHochsprung = 75 ,
        bWLauf400 = 91.7 ,
        bWHürden = 26.7 ,
        bWDiskuswurf = 3.0 ,
        bWStabhochsprung = 100 ,
        bWSpeerwurf = 3.8 ,
        bWLauf1500 = 535 ,
         
        cWsprint = 1.81 ,
        cWWeitsprung = 1.41 ,
        cWKugelstoß = 1.05 ,
        cWHochsprung = 1.348 ,
        cWLauf400 = 1.81 ,
        cWHürden = 1.835 ,
        cWDiskuswurf = 1.10 ,
        cWStabhochsprung = 1.35 ,
        cWSpeerwurf = 1.04 ,
        cWLauf1500 = 1.88 ;
    
        saveScoreToVariable();
    
    
    
     if (sprint != "") {
         PunkteSprint = Math.round(runningFormula(aWsprint, bWsprint, cWsprint, sprint));
         document.querySelector('[name="100m_score"]').value = PunkteSprint;
     }
    else {
        PunkteSprint = 0;
        document.querySelector('[name="100m_score"]').value = "0";
    }
    
    if (Weitsprung != "") {
         PunkteWeitsprung = Math.round(jumpThrowFormula(aWWeitsprung, bWWeitsprung, cWWeitsprung, Weitsprung));
         document.querySelector('[name="long_jump_score"]').value = PunkteWeitsprung;
     }
    else {
        PunkteWeitsprung = 0;
        document.querySelector('[name="long_jump_score"]').value = "0";
    }
    
    if (Kugelstoß != "") {
         PunkteKugelstoß = Math.round(jumpThrowFormula(aWKugelstoß, bWKugelstoß, cWKugelstoß, Kugelstoß));
         document.querySelector('[name="shot_put_score"]').value = PunkteKugelstoß;
     }
    else {
        PunkteKugelstoß = 0;
        document.querySelector('[name="shot_put_score"]').value = "0";
    }
    
    if (Hochsprung != "") {
         PunkteHochsprung = Math.round(jumpThrowFormula(aWHochsprung, bWHochsprung, cWHochsprung, Hochsprung));
         document.querySelector('[name="high_jump_score"]').value = PunkteHochsprung;
     }
    else {
        PunkteHochsprung = 0;
        document.querySelector('[name="high_jump_score"]').value = "0";
    }
    
    if (Lauf400 != "") {
         PunkteLauf400 = Math.round(runningFormula(aWLauf400, bWLauf400, cWLauf400, Lauf400));
         document.querySelector('[name="400m_score"]').value = PunkteLauf400;
     }
    else {
        PunkteLauf400 = 0;
        document.querySelector('[name="400m_score"]').value = "0";
    }
    
    if (Hürden != "") {
         PunkteHürden = Math.round(runningFormula(aWHürden, bWHürden, cWHürden, Hürden));
         document.querySelector('[name="110m_hurdling_score"]').value = PunkteHürden;
     }
    else {
        PunkteHürden = 0;
        document.querySelector('[name="110m_hurdling_score"]').value = "0";
    }
    
    if (Diskuswurf != "") {
         PunkteDiskuswurf = Math.round(jumpThrowFormula(aWDiskuswurf, bWDiskuswurf, cWDiskuswurf, Diskuswurf));
         document.querySelector('[name="discus_throw_score"]').value = PunkteDiskuswurf
     }
    else {
        PunkteDiskuswurf = 0;
        document.querySelector('[name="discus_throw_score"]').value = "0";
    }
    
    if (Stabhochsprung != "") {
         PunkteStabhochsprung = Math.round(jumpThrowFormula(aWStabhochsprung, bWStabhochsprung, cWStabhochsprung, Stabhochsprung));
         document.querySelector('[name="pole_vault_score"]').value = PunkteStabhochsprung
     }
    else {
        PunkteStabhochsprung = 0;
        document.querySelector('[name="pole_vault_score"]').value = "0";
    }
    
    if (Speerwurf != "") {
         PunkteSpeerwurf = Math.round(jumpThrowFormula(aWSpeerwurf, bWSpeerwurf, cWSpeerwurf, Speerwurf));
         document.querySelector('[name="javelin_throw_score"]').value = PunkteSpeerwurf;
     }
    else {
        PunkteSpeerwurf = 0;
        document.querySelector('[name="javelin_throw_score"]').value = "0";
    }
    
    if (Lauf1500 != "") {
         PunkteLauf1500 = Math.round(runningFormula(aWLauf1500, bWLauf1500, cWLauf1500, Lauf1500));
         document.querySelector('[name="1500m_score"]').value = PunkteLauf1500; 
     }
    else {
        PunkteLauf1500 = 0;
        document.querySelector('[name="1500m_score"]').value = "0";
    }
    
     totalscoreM = PunkteSprint + PunkteWeitsprung + PunkteKugelstoß + PunkteHochsprung + PunkteLauf400
        + PunkteHürden + PunkteDiskuswurf + PunkteStabhochsprung + PunkteSpeerwurf + PunkteLauf1500;  
    
}   

/* Frauentabelle:

Wettkampf	A	B	C	Einheit e
100 m	17,857	21,0	1,81	1 s
Diskus	12,3311	3,0	1,10	1 m
Stabhochsprung	0,44125	100	1,35	1 cm
Speerwurf	15,9803	3,8	1,04	1 m
400 m	1,34285	91,7	1,81	1 s
100 m Hürden	9,23076	26,7	1,835	1 s
Weitsprung	0,188807	210	1,41	1 cm
Kugelstoß	56,0211	1,5	1,05	1 m
Hochsprung	1,84523	75	1,348	1 cm
1500 m	0,02883	535	1,88	1 s


*/
    

    
/* Männertabelle:


Wettkampf	A	B	C	Einheit e
100 m	25,4347	18,0	1,81	1 s
Weitsprung	0,14354	220	1,40	1 cm
Kugelstoß	51,39	1,5	1,05	1 m
Hochsprung	0,8465	75	1,42	1 cm
400 m	1,53775	82	1,81	1 s
110 m Hürden	5,74352	28,5	1,92	1 s
Diskus	12,91	4,0	1,1	1 m
Stabhochsprung	0,2797	100	1,35	1 cm
Speerwurf	10,14	7,0	1,08	1 m
1500 m	0,03768	480	1,85	1 s

*/
function calculateMen() {
    
        
    var aMsprint = 25.4347 ,
        aMWeitsprung =  0.14354 ,
        aMKugelstoß = 51.39 ,
        aMHochsprung = 0.8465 ,
        aMLauf400 = 1.53775 ,
        aMHürden = 5.74352 ,
        aMDiskuswurf = 	12.91 ,
        aMStabhochsprung = 0.2797 ,
        aMSpeerwurf = 10.14	,
        aMLauf1500 = 0.03768 ,
        
        bMsprint = 18.0 ,
        bMWeitsprung = 220 ,
        bMKugelstoß = 1.5 ,
        bMHochsprung = 75 ,
        bMLauf400 = 82 ,
        bMHürden = 28.5 ,
        bMDiskuswurf = 4.0 ,
        bMStabhochsprung = 100 ,
        bMSpeerwurf = 7.0 ,
        bMLauf1500 = 480 ,

        cMsprint = 1.81 ,
        cMWeitsprung = 1.40 ,
        cMKugelstoß = 1.05 ,
        cMHochsprung = 1.42 ,
        cMLauf400 = 1.81 ,
        cMHürden = 1.92 ,
        cMDiskuswurf = 1.1 ,
        cMStabhochsprung = 1.35 ,
        cMSpeerwurf = 1.08 ,
        cMLauf1500 = 1.85 ;
    
     saveScoreToVariable();
        
     if (sprint != "") {
         PunkteSprint = Math.round(runningFormula(aMsprint, bMsprint, cMsprint, sprint));
         document.querySelector('[name="100m_score"]').value = PunkteSprint;
     }
    else {
        PunkteSprint = 0;
        document.querySelector('[name="100m_score"]').value = "0";
    }
    
    if (Weitsprung != "") {
         PunkteWeitsprung = Math.round(jumpThrowFormula(aMWeitsprung, bMWeitsprung, cMWeitsprung, Weitsprung));
         document.querySelector('[name="long_jump_score"]').value = PunkteWeitsprung;
     }
    else {
        PunkteWeitsprung = 0;
        document.querySelector('[name="long_jump_score"]').value = "0";
    }
    
    if (Kugelstoß != "") {
         PunkteKugelstoß = Math.round(jumpThrowFormula(aMKugelstoß, bMKugelstoß, cMKugelstoß, Kugelstoß));
         document.querySelector('[name="shot_put_score"]').value = PunkteKugelstoß;
     }
    else {
        PunkteKugelstoß = 0;
        document.querySelector('[name="shot_put_score"]').value = "0";
    }
    
    if (Hochsprung != "") {
         PunkteHochsprung = Math.round(jumpThrowFormula(aMHochsprung, bMHochsprung, cMHochsprung, Hochsprung));
         document.querySelector('[name="high_jump_score"]').value = PunkteHochsprung;
     }
    else {
        PunkteHochsprung = 0;
        document.querySelector('[name="high_jump_score"]').value = "0";
    }
    
    if (Lauf400 != "") {
         PunkteLauf400 = Math.round(runningFormula(aMLauf400, bMLauf400, cMLauf400, Lauf400));
         document.querySelector('[name="400m_score"]').value = PunkteLauf400;
     }
    else {
        PunkteLauf400 = 0;
        document.querySelector('[name="400m_score"]').value = "0";
    }
    
    if (Hürden != "") {
         PunkteHürden = Math.round(runningFormula(aMHürden, bMHürden, cMHürden, Hürden));
         document.querySelector('[name="110m_hurdling_score"]').value = PunkteHürden;
     }
    else {
        PunkteHürden = 0;
        document.querySelector('[name="110m_hurdling_score"]').value = "0";
    }
    
    if (Diskuswurf != "") {
         PunkteDiskuswurf = Math.round(jumpThrowFormula(aMDiskuswurf, bMDiskuswurf, cMDiskuswurf, Diskuswurf));
         document.querySelector('[name="discus_throw_score"]').value = PunkteDiskuswurf
     }
    else {
        PunkteDiskuswurf = 0;
        document.querySelector('[name="discus_throw_score"]').value = "0";
    }
    
    if (Stabhochsprung != "") {
         PunkteStabhochsprung = Math.round(jumpThrowFormula(aMStabhochsprung, bMStabhochsprung, cMStabhochsprung, Stabhochsprung));
         document.querySelector('[name="pole_vault_score"]').value = PunkteStabhochsprung
     }
    else {
        PunkteStabhochsprung = 0;
        document.querySelector('[name="pole_vault_score"]').value = "0";
    }
    
    if (Speerwurf != "") {
         PunkteSpeerwurf = Math.round(jumpThrowFormula(aMSpeerwurf, bMSpeerwurf, cMSpeerwurf, Speerwurf));
         document.querySelector('[name="javelin_throw_score"]').value = PunkteSpeerwurf;
     }
    else {
        PunkteSpeerwurf = 0;
        document.querySelector('[name="javelin_throw_score"]').value = "0";
    }
    
    if (Lauf1500 != "") {
         PunkteLauf1500 = Math.round(runningFormula(aMLauf1500, bMLauf1500, cMLauf1500, Lauf1500));
         document.querySelector('[name="1500m_score"]').value = PunkteLauf1500; 
     }
    else {
        PunkteLauf1500 = 0;
        document.querySelector('[name="1500m_score"]').value = "0";
    }
    
    totalscoreW = PunkteSprint + PunkteWeitsprung + PunkteKugelstoß + PunkteHochsprung + PunkteLauf400
        + PunkteHürden + PunkteDiskuswurf + PunkteStabhochsprung + PunkteSpeerwurf + PunkteLauf1500;
    
     
}
    
    
 return {
        init: init,
        getScore: getScore,
        setScores: setScores,
        clearScores: clearScores,
        saveScoreToVariable: saveScoreToVariable,
    };
    
};
