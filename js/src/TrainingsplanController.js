var App = App || {};
App.TrainingsplanController = function (trainingsplan) {
    "use strict";
    /* eslint-env browser, jquery */
    
    var saveTrainButton,
        trainingszustand,
        boxes,
        boxProp,
        dataModel,
        boxcounter = 0,
        
        box1,
        box2,
        box3,
        box4,
        box5,
         box6,
         box7,
         box8,
         box9,
         box10,
         box11,
         box12,
         box13,
         box14,
         box15,
         box16,
         box17,
         box18,
         box19,
         box20,
         box21,
         box22,
         box23,
         box24,
         box25,
         box26,
         box27,
         box28,
         box29,
        box30,
        
        sprinttrain1 = $('#sprinttrain1'),
        sprinttrain2 = $('#sprinttrain2'),
        sprinttrain3 = $('#sprinttrain3'),
        weittrain1 = $('#weittrain1'),
        weittrain2 = $('#weittrain2'),
        weittrain3 = $('#weittrain3'),
        kugeltrain1 = $('#kugeltrain1'),
        kugeltrain2 = $('#kugeltrain2'),
        kugeltrain3 = $('#kugeltrain3'),
        hochtrain1 = $('#hochtrain1'),
        hochtrain2 = $('#hochtrain2'),
        hochtrain3 = $('#hochtrain3'),
        m400train1 = $('#m400train1'),
        m400train2 = $('#m400train2'),
        m400train3 = $('#m400train3'),
        hurdletrain1 = $('#hurdletrain1'),
        hurdletrain2 = $('#hurdletrain2'),
        hurdletrain3 = $('#hurdletrain3'),
        diskustrain1 = $('#diskustrain1'),
        diskustrain2 = $('#diskustrain2'),
        diskustrain3 = $('#diskustrain3'),
        stabtrain1 = $('#stabtrain1'),
        stabtrain2 = $('#stabtrain2'),
        stabtrain3 = $('#stabtrain3'),
        speertrain1 = $('#speertrain1'),
        speertrain2 = $('#speertrain2'),
        speertrain3 = $('#speertrain3'),
        m1500train1 = $('#m1500train1'),
        m1500train2 = $('#m1500train2'),
        m1500train3 = $('#m1500train3');
    
    
    function init() { 
    saveTrainButton = $('#saveTraining');
    saveTrainButton.on("click", onSaveTrainButtonClicked); 
    dataModel = App.Datamodel();
    
    initTrainingseinheitenClickable();
    
    }
    
    function initTrainingseinheitenClickable() {
        sprinttrain1.on("click", a);
        sprinttrain2.on("click", b);
        sprinttrain3.on("click", c);
        weittrain1.on("click", d);
        weittrain2.on("click", e);
        weittrain3.on("click", f);
        kugeltrain1.on("click", g);
        kugeltrain2.on("click", h);
        kugeltrain3.on("click", i);
        hochtrain1.on("click", j);
        hochtrain2.on("click", k);
        hochtrain3.on("click", l);
        m400train1.on("click", m);
        m400train2.on("click", n);
        m400train3.on("click", o);
        hurdletrain1.on("click", p);
        hurdletrain2.on("click", q);
        hurdletrain3.on("click", r);
        diskustrain1.on("click", s);
        diskustrain2.on("click", t);
        diskustrain3.on("click", u);
        stabtrain1.on("click", v);
        stabtrain2.on("click", w);
        stabtrain3.on("click", x);
        speertrain1.on("click", y);
        speertrain2.on("click", z);
        speertrain3.on("click", aa);
        m1500train1.on("click", bb);
        m1500train2.on("click", cc);
        m1500train3.on("click", dd);
        
    }  
    
    function a(event) {
        document.getElementById("trainingsname").innerHTML = "Sprinttraining 1";
        document.getElementById("trainingseinheit").innerHTML = "Koordination mit fließendem Übergang in Sprint (Geschwindigkeit aufnehmen ca. 10 m):<br>Dribbling → Skipping → Anfersen → Sprunglauf (3x)<br>6 x 30m Sprints → kurze Pause zwischen den Läufen immer nach 2 längere Pause und Fußgelenksprünge zur Lockerung<br>2 x 50m Sprints → kurze Pause zwischen den Läufen<br>1 x 80m Sprint";
        $('html, body').animate({scrollTop: $('#kugeltrain1').offset().top}, 700);
    }
    function b(event) {
        document.getElementById("trainingsname").innerHTML = "Sprinttraining 2";
        document.getElementById("trainingseinheit").innerHTML = "Sprinttraining 2 \r\n Koordination mit fließendem Übergang in Sprint (Geschwindigkeit aufnehmen ca. 10 m): Dribbling → Skipping → Anfersen → Sprunglauf (3x) \n Startblock einstellen \n 6 x Antritte aus dem Startblock ca. 10 m (Technik üben: langsames Aufrichten) \n 4 x 30m Sprints (Pause nach 2 Läufen) \r 1 x 80m Sprint";
        $('html, body').animate({scrollTop: $('#kugeltrain1').offset().top}, 700);
    }
    function c(event) {
        document.getElementById("trainingsname").innerHTML = "Sprinttraining 3";
        document.getElementById("trainingseinheit").innerHTML = "Sprinttraining 3 \n Startblock einstellen \n 4 x 30 m Sprints → 2 x 50 m Sprints → 2 x 100 m Sprints \n lange Pause (eventuell Wurftraining) \n 150 m Sprint → kurze Pause 200m Sprint → kurze Pause 150m Sprint";
        $('html, body').animate({scrollTop: $('#kugeltrain1').offset().top}, 700);
    }
    function d(event) {
        document.getElementById("trainingsname").innerHTML = "Weitsprungtraining 1";
        document.getElementById("trainingseinheit").innerHTML = "rhythmische Koordination mit Hütchen:<br>Hütchen in immer weiterem Abstand zur Beschleunigung → nach den Hütchen übergang in Sprint 10m<br>Anlauf austesten (vom Sprungbrett 10 Schritte laufen, Stelle markieren)<br>Sprungtraining:<br>5 Sprünge in die Sandgrube → kurze Pause<br>2x 5 beidbeinige Sprünge über kleine Hürden<br>Sprungtraining 2x wiederholen";
       $('html, body').animate({scrollTop: $('#kugeltrain1').offset().top}, 700);
    }
    function e(event) {
        document.getElementById("trainingsname").innerHTML = "Weitsprungtraining 2";
        document.getElementById("trainingseinheit").innerHTML = "rhythmische Koordination mit Hütchen:<br>Hütchen in immer weiterem Abstand zur Beschleunigung → nach den Hütchen übergang in Sprint 10m<br>10 Sprünge auf Weitsprungtechnik<br>Sprungkrafttraining: 10 niedrige Hindernisse aufstellen<br>- beidbeinige sprünge<br>- rechter Fuß<br>- linker Fuß<br>- Pferdchensprünge Absprung links<br>- Pferdchensprünge Abrsprung links<br>5 min Pause dann Sprungkraftprogramm wiederholen";
       $('html, body').animate({scrollTop: $('#kugeltrain1').offset().top}, 700);
    }
    function f(event) {
        document.getElementById("trainingsname").innerHTML = "Weitsprungtraining 3";
        document.getElementById("trainingseinheit").innerHTML = "rhythmische Koordination mit Hütchen:<br>Hütchen in immer weiterem Abstand zur Beschleunigung → nach den Hütchen übergang in Sprint 10m<br>Sprungtraining:<br>- 5 Sprünge auf Höhe mit Hinderniss vor der Grube<br>- 10 Wettkampfsprünge (ausreichend Pause zwischen den Sprüngen)"; 
       $('html, body').animate({scrollTop: $('#kugeltrain1').offset().top}, 700);
    }
    function g(event) {
        document.getElementById("trainingsname").innerHTML = "Kugelstoßtraining 1";
        document.getElementById("trainingseinheit").innerHTML = "Arme Dehnen mit der Kugel (8er um die Beine schwingen)<br>Wurftraining:<br>- 2 Schockwürfe aus den Beinen nach vorne<br>- 2 Schockwürfe aus den Beinen rückwärts (mit dem Rücken zum Wurfplatz)<br>- 5 Standwürfe auf Technik<br>- 20 min Krafttraining";
       $('html, body').animate({scrollTop: $('#kugeltrain1').offset().top}, 700);
    }
    function h(event) {
        document.getElementById("trainingsname").innerHTML = "Kugelstoßtraining 2";
        document.getElementById("trainingseinheit").innerHTML = "Arme Dehnen mit der Kugel (8er um die Beine schwingen)<br>Wurftraining:<br>- 2 Schockwürfe aus den Beinen nach vorne<br>- 2 Schockwürfe aus den Beinen rückwärts (mit dem Rücken zum Wurfplatz)<br>- 2 lockere Standwürfe<br>- für Fürtgeschrittene: 5 Würfe mit angleiten auf Technik<br>- für Anfänger: 5 weitere Standwürfe auf Weite<br>- 20 min Krafttraining";
       $('html, body').animate({scrollTop: $('#kugeltrain1').offset().top}, 700);
    }
    function i(event) {
        document.getElementById("trainingsname").innerHTML = "Kugelstoßtraining 3";
        document.getElementById("trainingseinheit").innerHTML = "Arme Dehnen mit der Kugel (8er um die Beine schwingen)<br>Wurftraining:<br>- 2 Schockwürfe aus den Beinen nach vorne<br>- 2 Schockwürfe aus den Beinen rückwärts (mit dem Rücken zum Wurfplatz)<br>- 5 Wettkampfwürfe<br>- 20 min Krafttraining";
       $('html, body').animate({scrollTop: $('#kugeltrain1').offset().top}, 700);
    }  
    function j(event) {
        document.getElementById("trainingsname").innerHTML = "Hochsprungtraining 1";
        document.getElementById("trainingseinheit").innerHTML = "Koordination im Kreis (beidseitig):<br>- Dribbling<br>- Skipping<br>- Kniehebelauf<br>- Hopserlauf<br>- Anfersen<br>Hochsprungtraining:<br>- 5 Schersprünge mit gesteigerter Höhe und 3 Schritten Anlauf<br>-Brücke machen auf dem Boden<br>- 2 Flopsprünge aus dem Stand direkt mit dem Rücken zur Matte<br>- 10 Flopsprünge auf Technik mit gesteigerter Höhe aus 7 Schritten Anlauf<br>Anlauf ausmessen";
        $('html, body').animate({scrollTop: $('#kugeltrain1').offset().top}, 700);
    }
    function k(event) {
        document.getElementById("trainingsname").innerHTML = "Hochsprungtraining 2";
        document.getElementById("trainingseinheit").innerHTML = "Koordination im Kreis (beidseitig):<br>- Dribbling<br>- Skipping<br>- Kniehebelauf<br>- Hopserlauf<br>- Anfersen<br>Hochsprungtraining:<br>- 5 Schersprünge mit gesteigerter Höhe und 3 Schritten Anlauf<br>- 2 Flopsprünge aus dem Stand direkt mit dem Rücken zur Matte<br>- 10 Flopsprünge mit gesteigerter Höhe aus 7 Schritten Anlauf";
        $('html, body').animate({scrollTop: $('#kugeltrain1').offset().top}, 700);
    }
    function l(event) {
        document.getElementById("trainingsname").innerHTML = "Hochsprungtraining 3";
        document.getElementById("trainingseinheit").innerHTML = "Koordination im Kreis (beidseitig):<br>- Dribbling<br>- Skipping<br>- Kniehebelauf<br>- Hopserlauf<br>- Anfersen<br>Hochsprungtraining:<br>- 5 Schersprünge mit gesteigerter Höhe und 3 Schritten Anlauf<br>- Brücke machen auf dem Boden<br>- 10-15 Wettkampfsprünge";
       $('html, body').animate({scrollTop: $('#kugeltrain1').offset().top}, 700);
    }
    function m(event) {
        document.getElementById("trainingsname").innerHTML = "400m Sprinttraining 1";
        document.getElementById("trainingseinheit").innerHTML = "Koordination mit fließendem Übergang in Sprint (Geschwindigkeit aufnehmen ca. 10 m):<br>Dribbling → Skipping → Anfersen → Sprunglauf (3x)<br>Tempoläufe:<br>- 600m<br>- 300m<br>- 600m<br>zwischen den Tempoläufen langsam gehen 5 min";
       $('html, body').animate({scrollTop: $('#kugeltrain1').offset().top}, 700);
    }
    function n(event) {
        document.getElementById("trainingsname").innerHTML = "400m Sprinttraining 2";
        document.getElementById("trainingseinheit").innerHTML = "Koordination mit fließendem Übergang in Sprint (Geschwindigkeit aufnehmen ca. 10 m):<br>Dribbling → Skipping → Anfersen → Sprunglauf (3x)<br>Intervalltraining:<br>4 Runden: 200m submaximaler Sprint → 200m langsam joggen<br>10 min Pause (eventuell lockere Kraftübungen) dann wiederholen.";
        $('html, body').animate({scrollTop: $('#kugeltrain1').offset().top}, 700);
    }
    function o(event) {
        document.getElementById("trainingsname").innerHTML = "400m Sprinttraining 3";
        document.getElementById("trainingseinheit").innerHTML = "Koordination mit fließendem Übergang in Sprint (Geschwindigkeit aufnehmen ca. 10 m):<br>Dribbling → Skipping → Anfersen → Sprunglauf (3x)<br>Tempoläufe: 3x 400m<br>zwischen den Läufen langsam gehen 5 min";
        $('html, body').animate({scrollTop: $('#kugeltrain1').offset().top}, 700);
    }
    function p(event) {
        document.getElementById("trainingsname").innerHTML = "Hürdentraining 1";
        document.getElementById("trainingseinheit").innerHTML = "Koordination mit fließendem Übergang in Sprint (Geschwindigkeit aufnehmen ca. 10 m):<br>Dribbling → Skipping → Anfersen → Sprunglauf (3x)<br>Hürdentraining:<br>- Hürden ganz eng stellen durchgehen (2x links, 2x rechts)<br>- Hürden weiter stellen: Skipping an den Hürden vorbei nur Nachziehbein (2x links 2x rechts, 7er Rhythmus)<br>- Skipping an den Hürden vorbei nur Schwungbein (2x links 2x rechts)<br>- Skipping mittig durch die Hürden (2x links 2x rechts)<br>- 5er Rhythmus: 2x links 2x rechts durchlaufen";
        $('html, body').animate({scrollTop: $('#kugeltrain1').offset().top}, 700);
    }
    function q(event) {
        document.getElementById("trainingsname").innerHTML = "Hürdentraining 2";
        document.getElementById("trainingseinheit").innerHTML = "Koordination mit fließendem Übergang in Sprint (Geschwindigkeit aufnehmen ca. 10 m):<br>Dribbling → Skipping → Anfersen → Sprunglauf (3x)<br>Hürdentraining:<br>- Hürden auf Wettkampfabstand: Skipping an den Hürden vorbei nur Nachziehbein (2x links 2x rechts, 7er Rhythmus)<br>- Skipping an den Hürden vorbei nur Schwungbein (2x links 2x rechts)<br>- Skipping mittig durch die Hürden (2x links 2x rechts)<br>- 5er Rhythmus: 2x links 2x rechts durchlaufen";
        $('html, body').animate({scrollTop: $('#kugeltrain1').offset().top}, 700);
    }
    function r(event) {
        document.getElementById("trainingsname").innerHTML = "Hürdentraining 3";
        document.getElementById("trainingseinheit").innerHTML = "Koordination mit fließendem Übergang in Sprint (Geschwindigkeit aufnehmen ca. 10 m):<br>Dribbling → Skipping → Anfersen → Sprunglauf (3x)<br>Hürdentraining:<br>- Hürdenauf Wettkampfabstand: Skipping an  Hürden vorbei nur Nachziehbein (1x links 1x rechts, 7er Rhythmus)<br>- Skipping an den Hürden vorbei nur Schwungbein (1x links 1x rechts)<br>- Skipping mittig durch die Hürden (1x links 1x rechts)<br>- 6 Wettkampfläufe mit 5 Hürden und Startblock";
        $('html, body').animate({scrollTop: $('#kugeltrain1').offset().top}, 700);
    }
    function s(event) {
        document.getElementById("trainingsname").innerHTML = "Diskuswurftraining 1";
        document.getElementById("trainingseinheit").innerHTML = "Vorübungen mit dem Diskus:<br>- Diskus nach vorne über Zeigefinger rollen<br>- Dikus nach oben werfen und wieder fangen<br>Wurftraining: 10 Standwürfe auf Technik<br>20 min Krafttraining";
        $('html, body').animate({scrollTop: $('#kugeltrain1').offset().top}, 700);
    }
    function t(event) {
        document.getElementById("trainingsname").innerHTML = "Diskuswurftraining 2";
        document.getElementById("trainingseinheit").innerHTML = "Vorübungen mit dem Diskus:<br>- Diskus nach vorne über Zeigefinger rollen<br>- Dikus nach oben werfen und wieder fangen<br>Wurftraining:<br>- 10 Standwürfe<br>- Drehung ausprobieren<br>20 min Krafttraining";
        $('html, body').animate({scrollTop: $('#kugeltrain1').offset().top}, 700);
    }
    function u(event) {
        document.getElementById("trainingsname").innerHTML = "Diskuswurftraining 3";
        document.getElementById("trainingseinheit").innerHTML ="Vorübungen mit dem Diskus:<br>- Diskus nach vorne über Zeigefinger rollen<br>- Dikus nach oben werfen und wieder fangen<br>Wurftraining: 10 Wettkampfwürfe (aus dem Stand oder mit Drehung)<br>20 min Krafttraining";
        $('html, body').animate({scrollTop: $('#kugeltrain1').offset().top}, 700);
    }
    function v(event) {
        document.getElementById("trainingsname").innerHTML = "Stabhochsprungtraining 1";
        document.getElementById("trainingseinheit").innerHTML = "Koordination mit dem Stab:<br>- Hopserlauf der 3. jeweils betont mit Abdruck, Stabwandert von der Hüfte immer weiter nach oben<br>- Steigerungen mit dem Stab 30m<br>Sprungtraining:<br>- 3 Sprünge in den Sandkasten mit 3 Schritten<br>- 5 Sprünge in den Sandkasten mit 5 Anlauf(immer höher greifen)<br>- 7-10 Schritte ausprobieren → Anlauf ausmessen";
        $('html, body').animate({scrollTop: $('#kugeltrain1').offset().top}, 700);
    }
    function w(event) {
        document.getElementById("trainingsname").innerHTML = "Stabhochsprungtraining 2";
        document.getElementById("trainingseinheit").innerHTML = "Koordination mit dem Stab:<br>- Hopserlauf der 3. jeweils betont mit Abdruck, Stabwandert von der Hüfte immer weiter nach oben<br>- Steigerungen mit dem Stab 30m<br>Sprungtraining:<br> 15 Sprünge auf die Matte mit gesteigerter Höhe";
        $('html, body').animate({scrollTop: $('#kugeltrain1').offset().top}, 700);
    }
    function x(event) {
        document.getElementById("trainingsname").innerHTML = "Stabhochsprungtraining 3";
        document.getElementById("trainingseinheit").innerHTML = "Koordination mit dem Stab:<br>- Hopserlauf der 3. jeweils betont mit Abdruck, Stabwandert von der Hüfte immer weiter nach oben<br>- Steigerungen mit dem Stab 30m<br>Sprungtraining:<br> 15 Wettkampfsprünge auf die Matte mit gesteigerter Höhe";
        $('html, body').animate({scrollTop: $('#kugeltrain1').offset().top}, 700);
    }
    function y(event) {
        document.getElementById("trainingsname").innerHTML = "Speerwurftraining 1";
        document.getElementById("trainingseinheit").innerHTML = "Arme Dehnen mit dem Speer<br>Wurftraining:<br>- 10 Steckwürfe mit dem Wurfarm<br>- 2 Steckwürfe mit dem anderen Arm<br>- 10 Bogenwürfe über dem Kopf<br>- 10 Standwürfe<br>- 3 Schritte Anlauf langsam<br>- 3 Schritte Anlauf zackig";
        $('html, body').animate({scrollTop: $('#kugeltrain1').offset().top}, 700);
    }
    function z(event) {
        document.getElementById("trainingsname").innerHTML = "Speerwurftraining 2";
        document.getElementById("trainingseinheit").innerHTML = "Arme Dehnen mit dem Speer<br>Wurftraining:<br>- 10 Steckwürfe mit dem Wurfarm<br>- 2 Steckwürfe mit dem anderen Arm<br>- 10 Bogenwürfe über dem Kopf<br>- 5 Standwürfe<br>- 5 Würfe mit 3 Schritten Anlauf<br>- 5 Schritte Anlauf langsam<br>- 5 Schritte Anlauf schnell";
        $('html, body').animate({scrollTop: $('#kugeltrain1').offset().top}, 700);
    }
    function aa(event) {
        document.getElementById("trainingsname").innerHTML = "Speerwurftraining 3";
        document.getElementById("trainingseinheit").innerHTML = "Arme Dehnen mit dem Speer<br>Wurftraining:<br>- 5 Steckwürfe mit dem Wurfarm<br>- 5 Bogenwürfe über dem Kopf<br>- 3 Standwürfe<br>- 7 Schritte Anlauf testen<br>- 10 Wettkampfwürfe mit 5 oder 7 Schritten Anlauf";
        $('html, body').animate({scrollTop: $('#kugeltrain1').offset().top}, 700);
    }
    function bb(event) {
        document.getElementById("trainingsname").innerHTML = "1500m Lauftraining 1";
        document.getElementById("trainingseinheit").innerHTML = "Koordination mit fließendem Übergang in Sprint (Geschwindigkeit aufnehmen ca. 10 m):<br>Dribbling → Skipping → Anfersen → Sprunglauf (3x)<br>Intervalltraining:<br>4 Runden: Kurven(100m) submaximaler Sprint → Geraden(100m) langsam joggen<br>10 min Pause (eventuell lockere Kraftübungen) dann wiederholen.";
       $('html, body').animate({scrollTop: $('#kugeltrain1').offset().top}, 700);
    }
    function cc(event) {
        document.getElementById("trainingsname").innerHTML = "1500m Lauftraining 2";
        document.getElementById("trainingseinheit").innerHTML = "Koordination mit fließendem Übergang in Sprint (Geschwindigkeit aufnehmen ca. 10 m):<br>Dribbling → Skipping → Anfersen → Sprunglauf (3x)<br>Tempoläufe: 3x 800m<br>zwischen den Läufen langsam gehen 5 min";
        $('html, body').animate({scrollTop: $('#kugeltrain1').offset().top}, 700);
    }
    function dd(event) {
        document.getElementById("trainingsname").innerHTML = "1500m Lauftraining 3";
        document.getElementById("trainingseinheit").innerHTML = "Koordination mit fließendem Übergang in Sprint (Geschwindigkeit aufnehmen ca. 10 m):<br>Dribbling → Skipping → Anfersen → Sprunglauf (3x)<br>Tempoläufe: 2x 1500m<br>zwischen den Läufen langsam gehen 5 min";
        $('html, body').animate({scrollTop: $('#kugeltrain1').offset().top}, 700);
    }
    
    
        
    function onSaveTrainButtonClicked(event) {
        saveTrainingszustand();
        trainingszustand = getTrainingszustand();
        dataModel.trainingDataUpload(trainingszustand);
    }
    
    function saveTrainingszustand() {
        checkBoxes();
        boxes = {"checkboxes" : [ {"sprint1" : box1 },
                {"sprint2" : box2},
                {"sprint3" : box3},
                {"weit1" : box4},
                {"weit2" : box5},
                {"weit3" : box6},
                {"kugel1" : box7},
                {"kugel2" : box8},
                {"kugel3" : box9},
                {"hoch1" : box10},
                {"hoch2" : box11},
                {"hoch3" : box12},
                {"m4001" : box13},
                {"m4002" : box14},
                {"m4003" : box15},
                {"hürden1" : box16},
                {"hürden2" : box17},
                {"hürden3" : box18},
                {"diskus1" : box19},
                {"diskus2" : box20},
                {"diskus3" : box21},
                {"stab1" : box22},
                {"stab2" : box23},
                {"stab3" : box24},
                {"speer1" : box25},
                {"speer2" : box26},
                {"speer3" : box27},
                {"m15001" : box28},
                {"m15002" : box29},
                {"m15003" : box30}                  
                ]};
        
       
    }
    
    function getTrainingszustand() {
        boxProp = JSON.stringify(boxes);
        return boxProp;   
    }
    
    function checkBoxes() {
        
         box1 = document.getElementById("CSprinttraining1").checked;
        if (box1) boxcounter ++;
         box2 = document.getElementById("CSprinttraining2").checked;
        if (box2) boxcounter ++;
         box3 = document.getElementById("CSprinttraining3").checked;
        if (box3) boxcounter ++;
         box4 = document.getElementById("CWeittraining1").checked;
        if (box4) boxcounter ++;
         box5 = document.getElementById("CWeittraining2").checked;
        if (box5) boxcounter ++;
         box6 = document.getElementById("CWeittraining3").checked;
        if (box6) boxcounter ++;
         box7 = document.getElementById("CKugeltraining1").checked;
        if (box7) boxcounter ++;
         box8 = document.getElementById("CKugeltraining2").checked;
        if (box8) boxcounter ++;
         box9 = document.getElementById("CKugeltraining3").checked;
        if (box9) boxcounter ++;
         box10 = document.getElementById("CHochtraining1").checked;
        if (box10) boxcounter ++;
         box11 = document.getElementById("CHochtraining2").checked;
        if (box11) boxcounter ++;
         box12 = document.getElementById("CHochtraining3").checked;
        if (box12) boxcounter ++;
         box13 = document.getElementById("C400mtraining1").checked;
        if (box13) boxcounter ++;
         box14 = document.getElementById("C400mtraining2").checked;
        if (box14) boxcounter ++;
         box15 = document.getElementById("C400mtraining3").checked;
        if (box15) boxcounter ++;
         box16 = document.getElementById("CHürdentraining1").checked;
        if (box16) boxcounter ++;
         box17 = document.getElementById("CHürdentraining2").checked;
        if (box17) boxcounter ++;
         box18 = document.getElementById("CHürdentraining3").checked;
        if (box18) boxcounter ++;
         box19 = document.getElementById("CDiskustraining1").checked;
        if (box19) boxcounter ++;
         box20 = document.getElementById("CDiskustraining2").checked;
        if (box20) boxcounter ++;
         box21 = document.getElementById("CDiskustraining3").checked;
        if (box21) boxcounter ++;
         box22 = document.getElementById("CStabtraining1").checked;
        if (box22) boxcounter ++;
         box23 = document.getElementById("CStabtraining2").checked;
        if (box23) boxcounter ++;
         box24 = document.getElementById("CStabtraining3").checked;
        if (box24) boxcounter ++;
         box25 = document.getElementById("CSpeertraining1").checked;
        if (box25) boxcounter ++;
         box26 = document.getElementById("CSpeertraining2").checked;
        if (box26) boxcounter ++;
         box27 = document.getElementById("CSpeertraining3").checked;
        if (box27) boxcounter ++;
         box28 = document.getElementById("C1500mtraining1").checked;
        if (box28) boxcounter ++;
         box29 = document.getElementById("C1500mtraining2").checked;
        if (box29) boxcounter ++;
         box30 = document.getElementById("C1500mtraining3").checked;
        if (box30) boxcounter ++;
        
         document.getElementById("trainingszustand").innerHTML = "Du hast schon " + boxcounter + " von insgesamt 30                 Trainingseinheiten absolviert!";
       
    
    }
    
    function resetBoxes() {
         
      document.getElementById("CSprinttraining1").checked = false;
         document.getElementById("CSprinttraining2").checked = false;
         document.getElementById("CSprinttraining3").checked = false;
         document.getElementById("CWeittraining1").checked = false;
         document.getElementById("CWeittraining2").checked = false;
         document.getElementById("CWeittraining3").checked = false;
         document.getElementById("CKugeltraining1").checked = false;
         document.getElementById("CKugeltraining2").checked = false;
         document.getElementById("CKugeltraining3").checked = false;
         document.getElementById("CHochtraining1").checked = false;
         document.getElementById("CHochtraining2").checked = false;
         document.getElementById("CHochtraining3").checked = false;
         document.getElementById("C400mtraining1").checked = false;
         document.getElementById("C400mtraining2").checked = false;
         document.getElementById("C400mtraining3").checked = false;
         document.getElementById("CHürdentraining1").checked = false;
         document.getElementById("CHürdentraining2").checked = false;
         document.getElementById("CHürdentraining3").checked = false;
         document.getElementById("CDiskustraining1").checked = false;
         document.getElementById("CDiskustraining2").checked = false;
         document.getElementById("CDiskustraining3").checked = false;
         document.getElementById("CStabtraining1").checked = false;
         document.getElementById("CStabtraining2").checked = false;
         document.getElementById("CStabtraining3").checked = false;
         document.getElementById("CSpeertraining1").checked = false;
         document.getElementById("CSpeertraining2").checked = false;
         document.getElementById("CSpeertraining3").checked = false;
         document.getElementById("C1500mtraining1").checked = false;
         document.getElementById("C1500mtraining2").checked = false;
         document.getElementById("C1500mtraining3").checked = false;
    
    }
    
    function setBoxes (boxarray) {

        
         document.getElementById("CSprinttraining1").checked = boxarray.checkboxes[0].sprint1;
         document.getElementById("CSprinttraining2").checked = boxarray.checkboxes[1].sprint2;
         document.getElementById("CSprinttraining3").checked = boxarray.checkboxes[2].sprint3;
         document.getElementById("CWeittraining1").checked = boxarray.checkboxes[3].weit1;
         document.getElementById("CWeittraining2").checked = boxarray.checkboxes[4].weit2;
         document.getElementById("CWeittraining3").checked = boxarray.checkboxes[5].weit3;
         document.getElementById("CKugeltraining1").checked = boxarray.checkboxes[6].kugel1;
         document.getElementById("CKugeltraining2").checked = boxarray.checkboxes[7].kugel2;
         document.getElementById("CKugeltraining3").checked = boxarray.checkboxes[8].kugel3;
         document.getElementById("CHochtraining1").checked = boxarray.checkboxes[9].hoch1;
         document.getElementById("CHochtraining2").checked = boxarray.checkboxes[10].hoch2;
         document.getElementById("CHochtraining3").checked = boxarray.checkboxes[11].hoch3;
         document.getElementById("C400mtraining1").checked = boxarray.checkboxes[12].m4001;
         document.getElementById("C400mtraining2").checked = boxarray.checkboxes[13].m4002;
         document.getElementById("C400mtraining3").checked = boxarray.checkboxes[14].m4003;
         document.getElementById("CHürdentraining1").checked = boxarray.checkboxes[15].hürden1;
         document.getElementById("CHürdentraining2").checked = boxarray.checkboxes[16].hürden2;
         document.getElementById("CHürdentraining3").checked = boxarray.checkboxes[17].hürden3;
         document.getElementById("CDiskustraining1").checked = boxarray.checkboxes[18].diskus1;
         document.getElementById("CDiskustraining2").checked = boxarray.checkboxes[19].diskus2;
         document.getElementById("CDiskustraining3").checked = boxarray.checkboxes[20].diskus3;
         document.getElementById("CStabtraining1").checked = boxarray.checkboxes[21].stab1;
         document.getElementById("CStabtraining2").checked = boxarray.checkboxes[22].stab2;
         document.getElementById("CStabtraining3").checked = boxarray.checkboxes[23].stab3;
         document.getElementById("CSpeertraining1").checked = boxarray.checkboxes[24].speer1;
         document.getElementById("CSpeertraining2").checked = boxarray.checkboxes[25].speer2;
         document.getElementById("CSpeertraining3").checked = boxarray.checkboxes[26].speer3;
         document.getElementById("C1500mtraining1").checked = boxarray.checkboxes[27].m15001;
         document.getElementById("C1500mtraining2").checked = boxarray.checkboxes[28].m15002;
         document.getElementById("C1500mtraining3").checked = boxarray.checkboxes[29].m15003;
        
       
        
    }
    
    
    return {
        init: init,
        setBoxes: setBoxes,
        resetBoxes: resetBoxes
    }
    
    
};